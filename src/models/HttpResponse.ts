export default interface HttpResponse {
  /**
   * The HTTP request's response headers.
   *
   * @see https://developer.mozilla.org/en-US/docs/Web/API/Response/headers
   */
  headers: object;

  /**
   * A boolean indicating whether the response
   * was successful (status in the range 200–299) or not.
   */
  ok: boolean;

  /**
   * Indicates whether or not the response is the result of a redirect.
   *
   * @see https://developer.mozilla.org/en-US/docs/Web/API/Response/redirected
   */
  redirected: boolean;

  /**
   * The status code of the response. (This will be 200 for a success).
   */
  status: number;

  /**
   * The status message corresponding to the status code. (e.g., OK for 200).
   */
  statusText: string;

  /**
   * The type of the response (e.g., basic, cors).
   *
   * @see https://developer.mozilla.org/en-US/docs/Web/API/Response/type
   */
  type: string;

  /**
   * The URL of the response.
   *
   * @see https://developer.mozilla.org/en-US/docs/Web/API/Response/url
   */
  url: string;

  /**
   * Response body. Can be an object deserialized from JSON or plain text.
   */
  body: object | string;
}
