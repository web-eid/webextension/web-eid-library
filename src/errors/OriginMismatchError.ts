import ErrorCode from "./ErrorCode";

export default class OriginMismatchError extends Error {
  public code: ErrorCode;

  constructor(message = "URLs for a single operation require the same origin") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_ORIGIN_MISMATCH;
  }
}
