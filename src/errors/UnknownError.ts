import ErrorCode from "./ErrorCode";

export default class UnknownError extends Error {
  public code: ErrorCode;

  constructor(message = "an unknown error occurred") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_UNKNOWN_ERROR;
  }
}
