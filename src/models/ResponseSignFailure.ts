import Message from "./Message";
import HttpResponse from "./HttpResponse";

export default interface ResponseSignFailure extends Message {
  code:      string;
  response?: HttpResponse;
}
