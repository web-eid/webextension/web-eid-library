export default interface Versions {
  library:    string;
  nativeApp?: string;
  extension?: string;
}
