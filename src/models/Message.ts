export default interface Message {
  action: string;

  [key: string]: any;
}
