import Message from "./Message";
import HttpResponse from "./HttpResponse";

export default interface ResponseAuthenticateFailure extends Message {
  code:      string;
  response?: HttpResponse;
}
