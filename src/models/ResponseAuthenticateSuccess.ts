import Message from "./Message";
import HttpResponse from "./HttpResponse";

export default interface ResponseAuthenticateSuccess extends Message {
  response: HttpResponse;
}
