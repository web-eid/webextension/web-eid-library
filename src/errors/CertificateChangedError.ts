import ErrorCode from "./ErrorCode";

export default class CertificateChangedError extends Error {
  public code: ErrorCode;

  constructor(message = "server certificate changed between requests") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_CERTIFICATE_CHANGED;
  }
}
