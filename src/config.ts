export default Object.freeze({
  VERSION:                          "0.9.0",
  EXTENSION_HANDSHAKE_TIMEOUT:      1000,          // 1 second
  NATIVE_APP_HANDSHAKE_TIMEOUT:     5 * 1000,      // 5 seconds
  DEFAULT_USER_INTERACTION_TIMEOUT: 2 * 60 * 1000, // 2 minutes
  DEFAULT_SERVER_REQUEST_TIMEOUT:   20 * 1000,     // 20 seconds
});
