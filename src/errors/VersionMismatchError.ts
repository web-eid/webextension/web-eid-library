import ErrorCode from "./ErrorCode";
import Versions from "../models/Versions";
import RequiresUpdate from "../models/RequiresUpdate";

function tmpl(strings: TemplateStringsArray, requiresUpdate: string): string {
  return `Update required for Web-eID ${requiresUpdate}`;
}

export default class VersionMismatchError extends Error {
  public requiresUpdate: RequiresUpdate;
  public code: ErrorCode;
  public versions?: Versions;
  public nativeApp?: string;
  public extension?: string;
  public library?: string;

  constructor(message: string | undefined, versions: Versions, requiresUpdate: RequiresUpdate) {
    if (!message) {
      if (!requiresUpdate) {
        message = "requiresUpdate not provided";
      } else if (requiresUpdate.extension && requiresUpdate.nativeApp) {
        message = tmpl`${"extension and native app"}`;
      } else if (requiresUpdate.extension) {
        message = tmpl`${"extension"}`;
      } else if (requiresUpdate.nativeApp) {
        message = tmpl`${"native app"}`;
      }
    }

    super(message);

    this.name           = this.constructor.name;
    this.code           = ErrorCode.ERR_WEBEID_VERSION_MISMATCH;
    this.requiresUpdate = requiresUpdate;

    if (versions) {
      const { library, extension, nativeApp } = versions;

      Object.assign(this, { library, extension, nativeApp });
    }
  }
}
