import Message from "./Message";

export default interface ResponseStatusFailure extends Message {
  code:       string;
  message?:   string;
  extension:  string;
  nativeApp?: string;
}
