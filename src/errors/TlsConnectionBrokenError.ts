import ErrorCode from "./ErrorCode";

export default class TlsConnectionBrokenError extends Error {
  public code: ErrorCode;

  constructor(message = "TLS connection was broken") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_TLS_CONNECTION_BROKEN;
  }
}
