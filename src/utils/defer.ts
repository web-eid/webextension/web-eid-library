export default function defer(): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve));
}
