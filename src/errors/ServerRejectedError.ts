import ErrorCode from "./ErrorCode";

export default class ServerRejectedError extends Error {
  public code: ErrorCode;
  public response?: Response;

  constructor(message = "server rejected the request") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_SERVER_REJECTED;
  }
}
