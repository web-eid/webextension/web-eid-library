enum Action {
  STATUS         = "web-eid:status",
  STATUS_ACK     = "web-eid:status-ack",
  STATUS_SUCCESS = "web-eid:status-success",
  STATUS_FAILURE = "web-eid:status-failure",

  AUTHENTICATE         = "web-eid:authenticate",
  AUTHENTICATE_ACK     = "web-eid:authenticate-ack",
  AUTHENTICATE_SUCCESS = "web-eid:authenticate-success",
  AUTHENTICATE_FAILURE = "web-eid:authenticate-failure",

  SIGN         = "web-eid:sign",
  SIGN_ACK     = "web-eid:sign-ack",
  SIGN_SUCCESS = "web-eid:sign-success",
  SIGN_FAILURE = "web-eid:sign-failure",
}

export default Action;
