import ErrorCode from "./ErrorCode";

export default class TlsConnectionWeakError extends Error {
  public code: ErrorCode;

  constructor(message = "TLS connection was weak") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_TLS_CONNECTION_WEAK;
  }
}
