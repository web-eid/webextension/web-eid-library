import Message from "./Message";
import HttpResponse from "./HttpResponse";

export default interface ResponseSignSuccess extends Message {
  response: HttpResponse;
}
