import ErrorCode from "./ErrorCode";

export default class ServerTimeoutError extends Error {
  public code: ErrorCode;

  constructor(message = "server failed to respond in time") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_SERVER_TIMEOUT;
  }
}
