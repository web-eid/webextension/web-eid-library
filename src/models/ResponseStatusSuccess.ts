import Message from "./Message";

export default interface ResponseStatusSuccess extends Message {
  extension: string;
  nativeApp: string;
}
