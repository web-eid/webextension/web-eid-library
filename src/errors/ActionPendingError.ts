import ErrorCode from "./ErrorCode";

export default class ActionPendingError extends Error {
  public code: ErrorCode;

  constructor(message = "same action for Web-eID browser extension is already pending") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_ACTION_PENDING;
  }
}
