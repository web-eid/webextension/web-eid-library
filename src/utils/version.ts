import Versions from "../models/Versions";
import { parseSemver, compareSemver, IdentifierDiff } from "./semver";
import UpdateRequired from "../models/RequiresUpdate";

/**
 * Checks if update is required.
 *
 * @param version Object containing SemVer version strings for library, extension and native app.
 *
 * @returns Object which specifies if the extension or native app should be updated.
 */
export function checkCompatibility(version: Versions): UpdateRequired {
  const library   = parseSemver(version.library);
  const extension = parseSemver(version.extension);
  const nativeApp = parseSemver(version.nativeApp);

  const extensionDiff = compareSemver(extension, library);
  const nativeAppDiff = compareSemver(nativeApp, library);

  return {
    extension: extensionDiff.major === IdentifierDiff.OLDER,
    nativeApp: nativeAppDiff.major === IdentifierDiff.OLDER,
  };
}

/**
 * Checks an object if 'library', 'extension' or 'nativeApp' properties are present.
 * Values are not checked for SemVer validity.
 *
 * @param object Object which will be checked for version properties.
 *
 * @returns Were any of the version properties found in the provided object.
 */
export function hasVersionProperties(object: any): boolean {
  if (typeof object === "object") {
    for (const prop of ["library", "extension", "nativeApp"]) {
      if (Object.hasOwnProperty.call(object, prop)) return true;
    }
  }

  return false;
}
