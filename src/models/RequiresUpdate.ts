export default interface RequiresUpdate {
  nativeApp: boolean;
  extension: boolean;
}
