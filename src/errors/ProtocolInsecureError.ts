import ErrorCode from "./ErrorCode";

export default class ProtocolInsecureError extends Error {
  public code: ErrorCode;

  constructor(message = "HTTPS required") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_PROTOCOL_INSECURE;
  }
}
