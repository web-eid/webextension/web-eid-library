import ErrorCode from "./ErrorCode";

export default class TlsConnectionInsecureError extends Error {
  public code: ErrorCode;

  constructor(message = "TLS connection was insecure") {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_TLS_CONNECTION_INSECURE;
  }
}
