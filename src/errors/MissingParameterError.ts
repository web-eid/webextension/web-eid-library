import ErrorCode from "./ErrorCode";

export default class MissingParameterError extends Error {
  public code: ErrorCode;

  constructor(message: string) {
    super(message);

    this.name = this.constructor.name;
    this.code = ErrorCode.ERR_WEBEID_MISSING_PARAMETER;
  }
}
