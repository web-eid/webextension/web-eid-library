import Message from "./Message";

export default interface PendingMessage {
  message:     Message;
  promise?:    Promise<Message>;
  resolve?:    Function;
  reject?:     Function;
  ackTimer?:   number;
  replyTimer?: number;
}
